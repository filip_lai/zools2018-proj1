#include "../include/Factory.h"

User* Factory::createUser(string nickName, string fullName)
{
	return new User(nickName, fullName);
}

Message* Factory::createMsg(string sentBy, string sentTo, string text, time_t timeStamp, bool isRead)
{
	return new Message(sentBy, sentTo, text, timeStamp, isRead);
}

Group* Factory::createGroup(string name, User* admin)
{
	return new Group(name, admin);
}