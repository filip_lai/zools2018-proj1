
#include "../include/GrReadMsg.h"

void GrReadMsg::use(uChat* chat)
{
	string text = "";

	cout << "\n"
	<< "==== GROUP /" << chat->getCurrentGroup()->getName() << "/ messages:"<< endl;
	for ( Message* eachMsg : chat->getCurrentGroup()->getMsgs() )
	{
		time_t msgTime = eachMsg->getTime();
        string dateTime = ctime(&msgTime);
        dateTime = dateTime.substr(0, dateTime.length()-1);

		cout << dateTime << " || " << eachMsg->getSentBy() << ": " << eachMsg->getText() << endl;
	}
	
}

string GrReadMsg::getInfo()
{
	return "Read Messages in Groupchat";
}