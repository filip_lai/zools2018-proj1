#include "uChat.h"

#include "GrAddUser.h"
#include "GrCreateMsg.h"
#include "GrReadMsg.h"
#include "GrRemoveUser.h"

uChat::uChat()
{
	db = new Database();
	m_currentUser = NULL;
}

void uChat::setCurrentUser(User* usr)
{
	m_currentUser = usr;
}

User* uChat::getCurrentUser()
{
	if (m_currentUser==NULL)
	{
		cout << "Error, no user set" << endl;
		return NULL;
	}
	return m_currentUser;
}

void uChat::setCurrentGroup(Group* grp)
{
	m_currentGroup = grp;
}

Group* uChat::getCurrentGroup()
{
	if (m_currentGroup==NULL)
	{
		cout << "Error, no group set" << endl;
		return NULL;
	}
	return m_currentGroup;	
}

Database* uChat::getDb()
{
	return db;
}

EMode uChat::MainMenu()
{
	string inputStr = "";
    int inputInt = 0;
    stringstream inStream;
   	EMode NEXTMODE;

    while(true)
    {
    	cout << "\n"
    	<< "==== MAINMENU\n"
	    << "-- Type:\n"
	    << "[1] to Login\n"
	    << "[2] to Register\n"
	    << "[3] to Exit" << endl;

	    getline(cin, inputStr);
	    inStream.str( std::string() );
	    inStream.clear();
	    inStream << inputStr;

	    // Check input for valid number value
	    if (inStream >> inputInt) // its a number
	    {
	        if (inputInt == 1)
	        {
	            return (NEXTMODE = M_LOGIN);
	        }
	        else if (inputInt == 2)
	        {
	            return (NEXTMODE = M_REGISTER);
	        }
	        else if (inputInt == 3)
	        {
	            return (NEXTMODE = M_EXIT);
	        }
	    }
	    cout << "Invalid number, please try again" << endl;
    }
}

EMode uChat::Register()
{
	EMode NEXTMODE;
	int inputInt = 0;

	string newNickName;
	string newFullName;
	stringstream inStream;

	cout << "\n"
    << "==== REGISTER\n"
    << "-- Please enter a Nickname or\n" 
    << "-- [1] to Return to main menu" << endl;

    getline(cin, newNickName);
    inStream.str( std::string() );
    inStream.clear();
    inStream << newNickName;

    // Return to Menu
    if (inStream >> inputInt) // its a number
    {
        if (inputInt == 1)
        {
            return (NEXTMODE = M_MAINMENU);    
        }
    }
    // User with same nickname exists
    if (db->searchUser(newNickName))
    {
        cout << "User with nickname: '" << newNickName << "' already exists" << endl;
        return (NEXTMODE = M_REGISTER);
    }

    cout << "-- Please enter your Fullname:" << endl;
    
    getline(cin, newFullName);

    db->addUser(newNickName, newFullName);
    cout << "New user [" << newNickName << ", " << newFullName << "] registered." << endl;

    return (NEXTMODE = M_MAINMENU);
}

EMode uChat::Login()
{
	EMode NEXTMODE;

    string inputStr = "";
    int inputInt = 0;
    stringstream inStream;

    while(true)
    {
    	cout << "\n"
        << "==== Logging in...\n"
        << "-- Registered Users:" << endl;  
        for(User* eachUser : db->getUsers()) 
            cout << eachUser->getNickName() << endl;
    
        cout << "\n"
        << "-- Type in your Nickname or\n" 
        << "-- [1] to Return to main menu" << endl; 
    
        getline(cin, inputStr);
        inStream.str( std::string() );
        inStream.clear();
        inStream << inputStr;
    
        // Return to Menu
        if (inStream >> inputInt) // its a number
        {
            if (inputInt == 1)
            {
                return (NEXTMODE = M_MAINMENU);
            }
        }
    
        bool found = db->searchUser(inputStr);
        if (found) 
        {
            setCurrentUser(db->getUserByName(inputStr));
            return (NEXTMODE = M_MAIL);
        }
    
        cout << "User not found, please try again (Case Sensitive)" << endl;
    }
}

EMode uChat::Mail()
{
	EMode NEXTMODE;

	string inputStr = "";
    int inputInt = 0;
    stringstream inStream;

    while(true)
	{
		cout << "\n"
	    << "-- Logged in as: " << getCurrentUser()->getNickName() << "\n"
	    << "[1] to Send a new message\n"
	    << "[2] to View new messages\n"
	    << "[3] to View sent messages\n" 
	    << "[4] to Enter Groupchat\n" 
	    << "[5] to Return to main menu\n" << endl;
	
	    getline(cin, inputStr);
	    inStream.str( std::string() );
	    inStream.clear();
	    inStream << inputStr;
	
	    if (inStream >> inputInt) // its a number
	    {
	        if (inputInt == 1)
	        // COMPOSE A MESSAGE
	        {
	            cout << "\n"
	            << "-- Who is the recipient? (Nickname):" << endl;
	            string recipient;
	            getline(cin, recipient);
	
	            cout << "Message:" << endl;
	            getline(cin, inputStr);
	            time_t timeNow = time(0);
	
	            db->addMsg(getCurrentUser()->getNickName(), recipient,inputStr,timeNow,false);
	            cout << "Message sent" << endl;
	        }

	        else if (inputInt == 2)
	        // SHOW NEW MESSAGES
	        {
	            cout << "\n"
	            << "#### New messages: " << endl;
	            for(Message* eachMsg : db->searchNewMsgs(getCurrentUser()))
	            {
	                cout << "-- Sender: " << eachMsg->getSentBy()
	                << "\n-- Text: " << eachMsg->getText() << endl;
	                time_t msgTime = eachMsg->getTime();
	                char* dateTime = ctime(&msgTime);
	                cout << "-- Time: " << dateTime << endl;
	
	                eachMsg->setIsRead();
	            }        
	        }

	        else if (inputInt == 3)
	        // SHOW SENT MESSAGES
	        {
	            cout << "\n"
	            << ">>>> Sent messages: " << endl;
	            for(Message* eachMsg : db->searchSentMsgs(getCurrentUser()))
	            {
	                cout << "-- Sent to: " << eachMsg->getSentTo()
	                << "\n-- Text: " << eachMsg->getText() << endl;
	                time_t msgTime = eachMsg->getTime();
	                char* dateTime = ctime(&msgTime);
	                cout << "-- Time: " << dateTime << endl;
	            }
	        }

	        else if (inputInt == 4)
	        {
	        	return (NEXTMODE = M_GROUPCHATMENU);
	        }

	        else if (inputInt == 5)
	        {
	            return (NEXTMODE = M_MAINMENU);
	        }
	    }

	    cout << "Invalid number, please try again" << endl;
	}
}

EMode uChat::GroupchatMenu()
{
	EMode NEXTMODE;

	string inputStr = "";
    int inputInt = 0;
    stringstream inStream;
    string newGroupName;

	while(true)
    {
    	cout << "\n"
	    << "==== Groupchat Menu:\n"
	    << "[1] to Enter a group\n"
	    << "[2] to Create a group\n"
	    << "[3] to Return to mailbox" << endl;

	    getline(cin, inputStr);
	    inStream.str( std::string() );
	    inStream.clear();
	    inStream << inputStr;

	    // Check input for valid number value
	    if (inStream >> inputInt) // its a number
	    {
	        if (inputInt == 1)
	        // ENTER A GROUP
	        {
	        	cout << "\n"
	    		<< "-- Existing groups:\n" << endl;
		        for( Group* eachGroup : db->getGroups() ) 
            		cout << eachGroup->getName() << endl;

            	cout << "\n"
            	<< "== Enter the name of group to join:" <<  endl;
            	getline(cin, inputStr);

            	bool found = db->searchGroup(inputStr);
            	if (found)
            	{
            		setCurrentGroup(db->getGroupByName(inputStr));
            		return (NEXTMODE = M_GROUPCHAT);
            	}
            	else
            		cout << "Invalid groupname - " << inputStr << endl;

	            return (NEXTMODE = M_GROUPCHATMENU);
	        }
	        else if (inputInt == 2)
	        // CREATE A NEW GROUP
	        {
	        	cout << "\n"
			    << "-- Enter a name for the group:"  << endl;
				getline(cin, newGroupName);

			    bool found = db->searchGroup(newGroupName);
			    if (found)
			    {
			    	cout << "Group with name '" << newGroupName << "' already exists." << endl;
			    }
			    else
			    {
			    	db->addGroup(newGroupName, getCurrentUser());

			    	db->getGroupByName(newGroupName)->addFunction(new GrReadMsg());	
					db->getGroupByName(newGroupName)->addFunction(new GrCreateMsg());
					db->getGroupByName(newGroupName)->addFunction(new GrAddUser());
					db->getGroupByName(newGroupName)->addFunction(new GrRemoveUser());
					
					db->getGroupByName(newGroupName)->addUser(getCurrentUser());
			    	cout << "Group [" << newGroupName << "] has been created" << endl;
			    }
			    	

	            return (NEXTMODE = M_GROUPCHATMENU);
	        }
	        else if (inputInt == 3)
	        // RETURN TO MAILBOX
	        {
	            return (NEXTMODE = M_MAIL);
	        }
	    }
	    cout << "Invalid number, please try again" << endl;
    }
}

EMode uChat::Groupchat()
{
	EMode NEXTMODE;
	vector<ChFunction*> options = getCurrentGroup()->getFunctions();
	int optionCount = options.size();

	string inputStr = "";
    int inputInt = 0;
    stringstream inStream;
   	

	while(true)
    {
    	cout << "\n"
	    << "==== Groupchat - " << getCurrentGroup()->getName() << endl;
	    for (int i = 0; i < optionCount; i++)
	    {
	    	cout << "[" << i << "] " << options.at(i)->getInfo() << endl;
	    }
	    cout << "[" << optionCount << "] Return to Groupchat Menu" << endl;

	    getline(cin, inputStr);
	    inStream.str( std::string() );
	    inStream.clear();
	    inStream << inputStr;

	    // Check input for valid number value
	    if (inStream >> inputInt) // its a number
	    {
	    	if (inputInt == optionCount)
	    	// RETURN TO GROUPCHAT MENU
	    	{
	    		setCurrentGroup(NULL);
	    		return (NEXTMODE = M_GROUPCHATMENU);
	    	}
	    	else if (inputInt < optionCount)
	    	// EXECUTE OPTIONS
	    	{
		        options.at(inputInt)->use(this);
	        	return (NEXTMODE = M_GROUPCHAT); 
	    	}
	    }
	    cout << "Invalid number, please try again" << endl;
    }
}