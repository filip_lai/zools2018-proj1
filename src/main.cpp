
#include "uChat.h"

int main() 
{
// **************** PROG START ****************
    cout << "\n"
    << "uChat initializing..." << endl;

    uChat* chat = new uChat();

    EMode NEXTMODE = M_MAINMENU;

    while (true)
    {
        switch(NEXTMODE)
        {
            // MAIN MENU
            case M_MAINMENU :
                NEXTMODE = chat->MainMenu();
            break;

            // LOGIN
            case M_LOGIN :
                NEXTMODE = chat->Login();
            break;

            // REGISTER
            case M_REGISTER :
                NEXTMODE = chat->Register();
            break;

            // EXIT
            case M_EXIT :
                cout << "EXITing uChat..." << endl;
            return 0;
            
            // MAIL
            case M_MAIL :
                NEXTMODE = chat->Mail();
            break;

            // GROUPCHATMENU
            case M_GROUPCHATMENU :
                NEXTMODE = chat->GroupchatMenu();
            break;

            // GROUPCHAT
            case M_GROUPCHAT :
                NEXTMODE = chat->Groupchat();
            break;
        }            
    }

    delete chat;
	return(0);
}