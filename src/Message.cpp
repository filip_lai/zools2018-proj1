#include "../include/Message.h"

Message::Message(string sentBy, string sentTo, string text, time_t timeStamp, bool isRead)
{
	m_sentBy = sentBy;
	m_sentTo = sentTo;
	m_text = text;
	m_timeStamp = timeStamp;
	m_isRead = isRead;
}

string Message::getSentBy()
{
	return m_sentBy;
}

string Message::getSentTo()
{
	return m_sentTo;
}

string Message::getText()
{
	return m_text;
}

time_t Message::getTime()
{
	return m_timeStamp;
}

bool Message::getIsRead()
{
	return m_isRead;
}

void Message::setIsRead()
{
	m_isRead = true;
}