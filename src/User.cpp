#include "../include/User.h"

User::User(string nickName, string fullName)
{
	m_nickName = nickName;
	m_fullName = fullName;
}

string User::getNickName()
{
	return m_nickName;
}

string User::getFullName()
{
	return m_fullName;
}