#include "../include/Database.h"

Database::Database()
{
	m_factory = new Factory();
}

void Database::addMsg(string sentBy, string sentTo, string text, time_t timeStamp, bool isRead)
{
	m_messages.push_back(m_factory->createMsg(sentBy, sentTo, text, timeStamp, isRead));
}

void Database::addUser(string nickName, string fullName)
{
	m_users.push_back(m_factory->createUser(nickName, fullName));
}

void Database::addGroup(string name, User* admin)
{
	m_groups.push_back(m_factory->createGroup(name, admin));
}

bool Database::searchUser(string searchNickName)
{
	bool found = false;
	for(User* eachUser : this->m_users)
	{
		if (eachUser->getNickName() == searchNickName)
		{
			found = true;
			break;
		}
    }
    return found;
}

bool Database::searchGroup(string groupName)
{
	bool found = false;
	for(Group* eachGroup : this->m_groups)
	{
		if (eachGroup->getName() == groupName)
		{
			found = true;
			break;
		}
	}
	return found;
}

vector<Message*> Database::searchNewMsgs(User* usr)
{
	vector<Message*> newMessages;
	newMessages.clear();

	for(Message* eachMsg : this->Database::getMsgs())
	{
		if ( (eachMsg->getSentTo() == usr->getNickName()) && (eachMsg->getIsRead() == false) )
		{
			newMessages.push_back(eachMsg);
		}
    }
    return newMessages;
}

vector<Message*> Database::searchSentMsgs(User* usr)
{
	vector<Message*> sentMessages;
	sentMessages.clear();

	for(Message* eachMsg : this->Database::getMsgs())
	{
		if ( eachMsg->getSentBy() == usr->getNickName() )
		{
			sentMessages.push_back(eachMsg);
		}
    }
    return sentMessages;
}


vector<User*> Database::getUsers()
{
	return m_users;
}

vector<Message*> Database::getMsgs()
{
	return m_messages;
}

vector<Group*> Database::getGroups()
{
	return m_groups;
}

User* Database::getUserByName(string nickName)
{
	for(User* eachUser : this->Database::getUsers())
	{
		if (eachUser->getNickName() == nickName)
			return eachUser;
	}
	return NULL;
}

Group* Database::getGroupByName(string name)
{
	for(Group* eachGroup : this->Database::getGroups())
	{
		if (eachGroup->getName() == name)
			return eachGroup;
	}
	return NULL;
}