
#include "../include/GrRemoveUser.h"

void GrRemoveUser::use(uChat* chat)
{
	string inputStr = "";

	cout << "\n"
	<< "Enter the nickname of User:" << endl;
	getline(cin, inputStr);

	bool found = chat->getDb()->searchUser(inputStr);
	if (found)
	// User exists in database
	{
		found = chat->getCurrentGroup()->searchUser(inputStr);
		if (found)
		// User exists in Group
		{
			chat->getCurrentGroup()->removeUser(inputStr);
/*
			for (int i = 0; i < currGroup->getUsers().size(); i++)
			{
				if (currGroup->getUsers().at(i)->getNickName() == inputStr)
				{
					cout << "User found at id: " << i <<endl;
					//currGroup->getUsers().erase(currGroup->getUsers().begin() + i);
					cout << "User /" << inputStr << "/ has been removed from the group" << endl; 
				}
			}
*/	
		}
		else
			cout << "User /" << inputStr << "/ is not in the group" << endl;  
	}
	else
		cout << "Invalid nickname" << endl;
}

string GrRemoveUser::getInfo()
{
	return "Remove User from Groupchat";
}

