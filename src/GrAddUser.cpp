
#include "../include/GrAddUser.h"



void GrAddUser::use(uChat* chat)
{
	string inputStr = "";

	cout << "\n"
	<< "Enter the nickname of User:" << endl;
	getline(cin, inputStr);

	bool found = chat->getDb()->searchUser(inputStr);
	if (found)
	// User exists in database
	{
		found = chat->getCurrentGroup()->searchUser(inputStr);
		if (found)
		// User is already in Group
			cout << "User " << inputStr << " is already in the group" << endl; 
		else
		{
			chat->getCurrentGroup()->addUser(chat->getDb()->getUserByName(inputStr));
			cout << "User /" << inputStr << "/ has been added to the group" << endl;  
		}
			
	}
	else
		cout << "Invalid nickname" << endl;
}

string GrAddUser::getInfo()
{
	return "Add a new User in Groupchat";
}


