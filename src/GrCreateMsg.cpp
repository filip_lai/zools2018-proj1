
#include "../include/GrCreateMsg.h"


void GrCreateMsg::use(uChat* chat)
{
	string text = "";

	cout << "\n"
	<< "Enter your message:" << endl;
	getline(cin, text);

	string userName = chat->getCurrentUser()->getNickName();
	string recipient = "/" + chat->getCurrentGroup()->getName() + "/";
    time_t timeNow = time(0);

    Message* newMsg = new Message(userName, recipient, text, timeNow, false);

    chat->getCurrentGroup()->addMsg(newMsg);

    cout << "Message added into group" << endl;
}

string GrCreateMsg::getInfo()
{
	return "Create a new Message in Groupchat";
}


