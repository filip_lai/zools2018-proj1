
#include "../include/Group.h"

Group::Group(string name, User* admin)
{
	m_name = name;
	m_admin = admin;
}

void Group::addFunction(ChFunction* newFunc)
{
	m_functions.push_back(newFunc);
}

void Group::addUser(User* usr)
{
	m_users.push_back(usr);
}

void Group::addMsg(Message* msg)
{
	m_messages.push_back(msg);
}

bool Group::searchUser(string nickName)
{
	bool found = false;
	for(User* eachUser : this->m_users)
	{
		if (eachUser->getNickName() == nickName)
		{
			found = true;
			break;
		}
    }
    return found;
}

string Group::getName()
{
	return m_name;
}

vector<User*> Group::getUsers()
{
	return m_users;
}

vector<Message*> Group::getMsgs()
{
	return m_messages;
}

vector<ChFunction*> Group::getFunctions()
{
	return m_functions;
}

void Group::removeUser(string nickName)
{
	for (int i = 0; i < m_users.size(); i++)
	{
		if (m_users.at(i)->getNickName() == nickName)
		{
			cout << "User found at id: " << i <<endl;
			m_users.erase(m_users.begin() + i);
			cout << "User /" << nickName << "/ has been removed from the group" << endl; 
		}
	}
}