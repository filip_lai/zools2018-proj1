#ifndef __Database_h__
#define __Database_h__

#include <vector>
#include "Factory.h"

class Database
{
private:
	vector<Message*> m_messages;
	vector<User*> m_users;
	vector<Group*> m_groups;
	Factory* m_factory;

public:
	Database();

	void addMsg(string sentBy, string sentTo, string text, time_t timeStamp, bool isRead);

	void addUser(string nickName, string fullName);

	void addGroup(string name, User* admin);

	bool searchUser(string nickName);

	bool searchGroup(string groupName);

	vector<Message*> searchNewMsgs(User* usr);

	vector<Message*> searchSentMsgs(User* usr);

	vector<User*> getUsers();

	vector<Message*> getMsgs();

	vector<Group*> getGroups();

	User* getUserByName(string nickName);

	Group* getGroupByName(string name);
};

#endif