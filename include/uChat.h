#ifndef __uChat_h__
#define __uChat_h__

#include <sstream>
#include <stdio.h>

#include "Database.h"

enum EMode
{  
	M_MAINMENU,
	M_REGISTER,
	M_LOGIN,
	M_MAIL,
	M_GROUPCHATMENU,
	M_GROUPCHAT,
	M_EXIT
};

class uChat
{
private:
	Database* db;
	User* m_currentUser;
	Group* m_currentGroup;

public:
	uChat();

	void setCurrentUser(User* usr);

	User* getCurrentUser();

	void setCurrentGroup(Group* grp);

	Group* getCurrentGroup();

	Database* getDb();

	EMode MainMenu();

	EMode Register();

	EMode Login();

	EMode Mail();

	EMode GroupchatMenu();

	EMode Groupchat();
};

#endif