#ifndef __ChFunction_h__
#define __ChFunction_h__

#include "uChat.h"

#include <string>
#include <iostream>
//#include <sstream>

using namespace std;

class ChFunction
{
public:
	virtual void use(uChat* chat) = 0;
	virtual string getInfo() = 0;
};

#endif