#ifndef __Group_h__
#define __Group_h__

#include <vector>
#include "Message.h"
#include "User.h"

class ChFunction;

class Group
{
private:
	string m_name;
	User* m_admin;
	vector<User*> m_users;
	vector<Message*> m_messages;
	vector<ChFunction*> m_functions;

public:
	Group(string name, User* admin);

	void addFunction(ChFunction* newFunc);

	void addUser(User* usr);

	void addMsg(Message* msg);

	bool searchUser(string nickName);

	string getName();

	vector<User*> getUsers();

	vector<Message*> getMsgs();

	vector<ChFunction*> getFunctions();

	void removeUser(string nickName);
};

#endif