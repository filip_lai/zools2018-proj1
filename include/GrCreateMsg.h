#ifndef __GrCreateMsg_h__
#define __GrCreateMsg_h__

#include "ChFunction.h"

class GrCreateMsg : public ChFunction
{
public:
	void use(uChat* chat);
	string getInfo();
};

#endif