#ifndef __User_h__
#define __User_h__

#include <iostream>
#include <string>
using namespace std;

class User
{
private:
	string m_nickName;
	string m_fullName;
	
public:
	User(string nickName, string fullName);

	string getNickName();

	string getFullName();
};

#endif