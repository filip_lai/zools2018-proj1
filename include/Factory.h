#ifndef __Factory_h__
#define __Factory_h__


#include "Message.h"
#include "User.h"
#include "Group.h"

class Factory
{
public:
	User* createUser(string nickName, string fullName);
	
	Message* createMsg(string sentBy, string sentTo, string text, time_t timeStamp, bool isRead);

	Group* createGroup(string name, User* admin);
};

#endif