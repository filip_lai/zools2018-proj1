#ifndef __GrAddUser_h__
#define __GrAddUser_h__

#include "ChFunction.h"

class GrAddUser : public ChFunction
{
public:
	void use(uChat* chat);
	string getInfo();
};

#endif