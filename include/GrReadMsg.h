#ifndef __GrReadMsg_h__
#define __GrReadMsg_h__

#include "ChFunction.h"

class GrReadMsg : public ChFunction
{
public:
	void use(uChat* chat);
	string getInfo();
};

#endif