#ifndef __Message_h__
#define __Message_h__

#include <string>
#include <time.h>
using namespace std;

class Message
{
private:
	string m_sentBy;
	string m_sentTo;
	string m_text;

	time_t m_timeStamp;
	bool m_isRead;

public:
	Message(string sentBy, string sentTo, string text, time_t timeStamp, bool isRead);

	string getSentBy();

	string getSentTo();

	string getText();

	time_t getTime();

	bool getIsRead();

	void setIsRead();
};

#endif