#ifndef __GrRemoveUser_h__
#define __GrRemoveUser_h__

#include "ChFunction.h"

class GrRemoveUser : public ChFunction
{
public:
	void use(uChat* chat);
	string getInfo();
};

#endif