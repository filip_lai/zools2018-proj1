#
#
#
#
# @name - '@' stops console output

# .PHONY - mark targets, that are Not files
.PHONY: all clean 

CXX=g++
SRC=src
IDIR=include
DEPS=Database.h Factory.h Message.h uChat.h User.h ChFunction.h GrAddUser.h GrCreateMsg.h Group.h GrReadMsg.h GrRemoveUser.h
OBJ=main.o Database.o Factory.o Message.o uChat.o User.o GrAddUser.o GrCreateMsg.o Group.o GrReadMsg.o GrRemoveUser.o
CXXFLAGS=-g -std=c++11 -Wall -pedantic

all: uChat

# '$^' - Right side of ':'
uChat: $(OBJ)
	$(CXX) -o uChat $^

# '$<' - First item in Dependencies list(Right side)
# '$@' - Target name (left side)
# $(IDIR)/$(DEPS)
%.o: $(SRC)/%.cpp 
	$(CXX) -c -o $@ $< -Iinclude $(CXXFLAGS)

clean:
	rm -f *.o uChat uChat.exe